MAX_MSG_NUM = 16
cur_msg_num = 0
$(document).ready(function() {
    var wsServer = "ws://localhost:8000/chat";
    // open websocket
    console.log("start socket connect...");
    var socket = new WebSocket(wsServer);
    // var socket = null;

    // prepare receive message
    recvMessage(socket);
    $("#speak-cont").select();

    // send message
    $('#send-btn').bind('click', function sendMessage() {
        console.log("send message...");
        cont = $("#speak-cont").val();
        socket.send(cont);
        $("#speak-cont").val("").select();
    });
});

function recvMessage(socket) {
    socket.onmessage = function (event) {
        data = JSON.parse(event.data);
        if (data.type == "login") {
            dom = '<li>欢迎<a class="nick">'+ data.nick +': </a><span>进入房间</span></li>';
        } else if (data.type == "text") {
            dom = '<li><a class="nick">'+ data.nick +': </a><span>' + data.content + '</span></li>';
        } else {
            dom = "text";
        };
        var chatDom = $("#chat-cont");
        if (cur_msg_num >= MAX_MSG_NUM) {
            chatDom.find('li').first().remove();
            cur_msg_num -= 1;
        }
        cur_msg_num += 1;
        console.log(cur_msg_num);
        chatDom.append(dom);
    };
};


