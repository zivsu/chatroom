
$(document).ready(function() {

    $("#enter").click(function(){
        var nick = $("#nick").val();
        if (nick == ""){
            alert("请输入昵称");
        } else {
            $.ajax({
                url:"login",
                data:JSON.stringify({nick: nick}),
                type:"POST",
                dataType:"json",
                success: function (data) {
                    resp_code = data.code;
                    if (resp_code == "0"){
                        window.location.href = "/room"
                    }else if (resp_code == "1002"){
                        alert("已存在该昵称");
                    }else {
                        alert("无效的请求");
                    }
                },
                error: function (data) {
                    alert("服务器繁忙,请稍后重试");
                }
            })
        }
    });
});