#!/usr/bin/env python

import logging
import uuid

from core.room import Room

class Hall(object):

    rooms = []
    max_rooms_num = 100

    def __init__(self):
        pass

    @property
    def rooms_num(self):
        return len(Hall.rooms)

    def add_one_room(self):
        if self.rooms_num == self.max_rooms_num:
            return None

        room = Room.new()
        Hall.rooms.append(room)
        return room

    def get_one_spare_room(self):
        # logging.info("rooms:{}".format(self.rooms))
        if self.rooms_num == 0:
            room = self.add_one_room()
            return room

        sorted(Hall.rooms, key=lambda room: len(room["users"]))
        for room in Hall.rooms:
            if Room.is_spare_room(room):
                return room

        # not spare room
        return self.add_one_room()