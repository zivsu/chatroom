#!/usr/bin/env python

import logging
import json
import uuid

MSG_TYPE_LOGIN = "login"
MSG_TYPE_TEXT = "text"

ws_clients = dict()

class Room(object):

    max_users_per_room = 25

    @classmethod
    def new(cls):
        room_id = str(uuid.uuid4())
        room = {
            "room_id": room_id,
            "users": []
        }
        return room

    @classmethod
    def is_spare_room(cls, room):
        return (len(room["users"]) < cls.max_users_per_room)

    @classmethod
    def add_one_user(cls, room, ws_client, user_info):
        room["users"].append(ws_client)

        user_nick = user_info["nick"]
        # broadcast message
        msg = Message(nick=user_nick, type=MSG_TYPE_LOGIN, content=None)
        cls.broadcast(room, msg)

    @classmethod
    def remove_one_user(cls, room, wssocket_id):
        try:
            room["users"].remove(wssocket_id)
        except:
            pass

    @classmethod
    def broadcast(cls, room, message):
        users = room["users"]
        for ws_client in users:
            try:
                ws_client.send_message(message)
            except:
                pass

class Message(dict):

    def __init__(self, *args, **kwargs):
        super(Message, self).__init__(*args, **kwargs)