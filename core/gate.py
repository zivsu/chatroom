#!/usr/bin/env python

import logging

from core.hall import Hall

class Gate(object):

    @classmethod
    def choose_room(cls):
        return Hall().get_one_spare_room()

    @classmethod
    def list_all_rooms(cls):
        return Hall.rooms