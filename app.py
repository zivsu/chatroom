#!/usr/bin/env python
# coding=utf-8

import os.path
import logging

import tornado.ioloop
import tornado.options
import tornado.web
import redis

from handlers.index import HallHandler, LoginHandler, RoomHandler
from handlers.chat import ChatHandler

from tornado.options import define, options

define("port", default=8000, help="run on the given port", type=int)


class Application(tornado.web.Application):

    def __init__(self, **kwargs):
        handlers = [
            (r"/", HallHandler),
            (r"/login", LoginHandler),
            (r"/room", RoomHandler),
            (r"/chat", ChatHandler)
        ]
        settings = dict(
            cookie_secret="5d4803c9-28ea-45ec-b818-2d5e049569f8",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            autoreload=True,
            # xsrf_cookies=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)
        self.redis_client = kwargs.get("redis_client")

if __name__ == "__main__":
    tornado.options.parse_command_line()
    port = options.port
    redis_client = redis.Redis(host='localhost', port=6379, db=0)
    app = Application(redis_client=redis_client)
    app.listen(options.port)
    logging.info("app init on {port} port".format(port=port))
    tornado.ioloop.IOLoop.current().start()