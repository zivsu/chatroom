#!/usr/bin/env python

import logging
import json
import uuid

import tornado.websocket

from core.room import Room, Message, MSG_TYPE_TEXT
from core.gate import Gate

class BaseWebSocketHandler(tornado.websocket.WebSocketHandler):

    @property
    def redis_client(self):
        return self.application.redis_client

class ChatHandler(BaseWebSocketHandler):

    def open(self):
        logging.info("websocket open")
        user_nick = self.get_secure_cookie("nick", None)
        if user_nick is None:
            return self.redirect("/login")

        self.room = Gate.choose_room()
        if self.room is None:
            # Not spare room
            return self.redirect("/login")
        logging.info(len(self.room["users"]))

        user_info = {"nick": user_nick}
        Room.add_one_user(self.room, self, user_info)

    def on_close(self):
        logging.info("websocket close")
        Room.remove_one_user(self.room, self)

    def on_message(self, message):
        logging.info("on message")
        user_nick = self.get_secure_cookie("nick", None)
        if user_nick is None:
            return self.redirect("/login")

        msg = Message(nick=user_nick, type=MSG_TYPE_TEXT, content=message)
        Room.broadcast(self.room, msg)

    def send_message(self, message):
        self.write_message(message)