#!/usr/bin/env python

import logging

import tornado.web
from tornado import escape

from core.gate import Gate

class BaseHandler(tornado.web.RequestHandler):

    @property
    def redis_client(self):
        return self.application.redis_client

class HallHandler(BaseHandler):

    def get(self):
        self.render("hall.html")

class LoginHandler(BaseHandler):

    def post(self):
        try:
            form = escape.json_decode(self.request.body)
        except Exception as e:
            logging.exception(e)
            return self.write({"code": "1001"})

        nick = form.get("nick", None)
        if nick is None:
            return self.write({"code": "1001"})

        self.set_secure_cookie("nick", nick)
        is_existed = self.redis_client.hexists("users", nick)
        if not is_existed:
            self.redis_client.hset("users", nick, True)
        self.write({"code": "0"})

class RoomHandler(BaseHandler):

    def get(self):
        self.render("room.html")